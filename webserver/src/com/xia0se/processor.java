package com.xia0se;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class processor extends Thread{
	private Socket socket;
	private InputStream in;
	private PrintStream out;
	private final static String WEB_ROOT="D:\\workspace\\webserver\\webroottest";
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		String filename = parse(in);
		sendFile(filename);
	}

	public processor(Socket socket){
		this.socket = socket;
		try {
			in = socket.getInputStream();
			out = new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public String parse(InputStream in){
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String filename = null;
		String httpMessage;
		
		try {
			httpMessage = br.readLine();
			String[] content = httpMessage.split(" ");
			
			if(content.length != 3){
				sendErrorMessage(400, "client query error");
				return null;
			}
			
			filename = content[1];
			System.out.println("code:" + content[0] + ", filename:" + content[1] + 
					", http version:" + content[2]);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return filename;
	}
	
	public void sendErrorMessage(int errorCode, String errorMessage){
		out.println("HTTP/1.0 " + errorCode + " " + errorMessage);
		out.println("content-type: text/html");
		out.println();
		out.println("<html>");
		out.println("<title> Error Message");
		out.println("</title>");
		out.println("<body>");
		out.println("<h1> ErrorCode:" + errorCode +",Message:" + errorMessage);
		out.println("</body>");
		out.println("</html>");
		out.flush();
		out.close();
		
	}
	
	public void sendFile(String filename){
		File file = new File(processor.WEB_ROOT + filename);
		if(!file.exists()){
			sendErrorMessage(404,"File not found!");
			return ;
		}
		
		try {
			InputStream in = new FileInputStream(file);
			byte content[] = new byte[((int)file.length())];
			in.read(content);
			out.println("HTTP/1,0 200 OK");
			out.println("content-length:" + content.length);
			out.println();
			out.write(content);
			out.flush();
			out.close();
			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
