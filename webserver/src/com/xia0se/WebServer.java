package com.xia0se;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class WebServer {


	public void serverStart(int port){
		try {
			ServerSocket serverSocket = new ServerSocket(port);
			
			while(true){
				Socket socket = serverSocket.accept();
				new processor(socket).start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//130722198602280022
		}
		

	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int port=80;
		if(args.length == 1){
			port = Integer.parseInt(args[0]);
		}
		
		new WebServer().serverStart(port);

	}

}
